﻿namespace OmegaBotWrapper.Enum
{
    /// <summary>
    /// Actions that can be taken during battle phase
    /// </summary>
    public enum BattleAction
    {
        Activate = 0,
        Attack = 1,
        ToMainPhaseTwo = 2,
        ToEndPhase = 3
    }
}