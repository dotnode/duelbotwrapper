﻿namespace OmegaBotWrapper.Struct
{
    /// <summary>
    /// Data about effect activation of given card
    /// </summary>
    public struct ActivationCardIndex
    {
        /// <summary>
        /// Description value of the effect
        /// </summary>
        public int ActionDescription;
        /// <summary>
        /// Action index of the effect
        /// </summary>
        public int ActionIndex;
    }
}
