﻿namespace OmegaBotWrapper.Struct
{
    /// <summary>
    /// Collection of multiple cards information from client database
    /// </summary>
    public struct DBCardList
    {
        /// <summary>
        /// Array of cards with their information from client
        /// </summary>
        public DBCardInfo[] Cards;
    }
}
